var json;
var net;
var trainer;

$("#import").click(function () {
    $('#result').val("");
    var files = document.getElementById('selectFiles').files;
    console.log(files);
    if (files.length <= 0) {
        return false;
    }
    var fr = new FileReader();

    fr.onload = function (e) {
        console.log(e);
        var result = JSON.parse(e.target.result);
        var formatted = JSON.stringify(result, null, 2);
        $('#result').val(formatted);
        console.log("coucou "+$('#result').val());
        json = JSON.parse($('#result').val());
        net = new convnetjs.Net();
        net.fromJSON(json);

        trainer = new convnetjs.SGDTrainer(net, {learning_rate: 0.01, momentum: 0.2, batch_size: 1, l2_decay: 0.001});
    }
    fr.readAsText(files.item(0));
});



$('#reset').click(function () {
    var canvas = document.getElementById('imageView');
    var ctx = canvas.getContext('2d');
    /*ctx.canvas.width  = 120;
    ctx.canvas.height = 120;*/
    ctx.clearRect(0, 0, 112, 112);
    ctx.beginPath();
    ctx.rect(0, 0, 112, 112);
    ctx.fillStyle = "black";
    ctx.fill();
});

$('#result').change(function(){
    console.log($('#result').val());
});

$('#some_image2').click(function () {
    var canvas = document.getElementById('imageView');
    context = canvas.getContext('2d');
    // resizeCanvas();
    // resizes along with the browser window.
    /*function redraw() {
        context.strokeStyle = 'blue';
        context.fillStyle = "black";
        context.lineWidth = '5';
        context.strokeRect(0, 0, 24, 24);
    }*/

    // Runs each time the DOM window resize event fires.
    // Resets the canvas dimensions to match window,
    // then draws the new borders accordingly.
    /*function resizeCanvas() {
        canvas.width = 24;
        canvas.height = 24;
        redraw();
    }*/


    var image = new Image();

    //image.src = canvas.toDataURL("image/png");

    image.src = resizeImage(canvas.toDataURL("image/png"));
    setTimeout(function(){
        image.src = resizeImage(canvas.toDataURL("image/png"));
        console.log("La nouvelle image "+image.src);
        $( "#result_ocr" ).after(image);
        setTimeout(startOCR, 1000);
    }, 3000);



    function resizeImage(dataURL) {
        var imageType = "image/png";
        var imageArguments = imageArguments || 0.7;
        // Create a temporary image so that we can compute the height of the downscaled image.
        var newWidth = 28;
        var image2 = new Image();
        image2.src = dataURL;
        var oldWidth = image2.width;
        var oldHeight = image2.height;
        var newHeight = Math.floor(oldHeight / oldWidth * newWidth);

        // Create a temporary canvas to draw the downscaled image on.
        var canvas2 = document.createElement("canvas");
        canvas2.width = newWidth;
        canvas2.height = newHeight;

        // Draw the downscaled image on the canvas and return the new data URL.
        var ctx = canvas2.getContext("2d");
        ctx.drawImage(image2, 0, 0, newWidth, newHeight);
        var newDataUrl = canvas2.toDataURL("image/png");
        return newDataUrl;
    }

// After you are done styling it, append it to the BODY element


 /*   var json = JSON.parse($('#result').val());
    var net = new convnetjs.Net();
    net.fromJSON(json);

    var trainer = new convnetjs.SGDTrainer(net, {learning_rate: 0.01, momentum: 0.2, batch_size: 1, l2_decay: 0.001});*/
    function startOCR() {
        var alphaOrNum = $('input[name=choix]:checked').val();

        layer_defs = [];
        layer_defs.push({type: 'input', out_sx: 24, out_sy: 24, out_depth: 9});
        layer_defs.push({type: 'conv', sx: 5, filters: 8, stride: 1, pad: 2, activation: 'relu'});
        layer_defs.push({type: 'pool', sx: 2, stride: 2});
        layer_defs.push({type: 'conv', sx: 5, filters: 16, stride: 1, pad: 2, activation: 'relu'});
        layer_defs.push({type: 'pool', sx: 3, stride: 3});
        layer_defs.push({type: 'softmax', num_classes: 10});

//net = new convnetjs.Net();
//net.makeLayers(layer_defs);

//trainer = new convnetjs.SGDTrainer(net, {method:'adadelta', batch_size:20, l2_decay:0.001});


// helpful utility for converting images into Vols is included
        //$( "#result" ).after(image);
        var x = convnetjs.img_to_vol(image);

        var probability_volume = net.forward(x);

        var temp_max = 0, class_max = 0;
        for(var i = 0; i < probability_volume.w.length; i++) {

            console.log('probability that x is class '+i+' : ' + probability_volume.w[i]);

            if(probability_volume.w[i] > temp_max) {
                temp_max = probability_volume.w[i];
                class_max = i;
            }
        }

        var symbols = [
            'a', 'A', 'b', 'B', 'Cc', 'd', 'D', 'e', 'E', 'f', 'F', 'g', 'G', 'h', 'H',
            'iI1l', 'Jj', 'k', 'K', 'L', 'm', 'M', 'n', 'N', 'Oo0', 'Pp', 'q', 'Q', 'r',
            'R', 'Ss', 't', 'T', 'Uu', 'vV', 'Ww', 'Xx', 'Yy', 'Zz', '3', '4', '7', '8'];
        console.log("Le maximum est : " + temp_max+" de la classe : "+class_max +" ---> "+symbols[class_max]);

        var result = jQuery.inArray( Math.max.apply(Math,probability_volume.w), probability_volume.w );
        var pourcent = Math.max.apply(Math,probability_volume.w)*100;

        if(alphaOrNum == "chiffre"){
            $('#result_ocr').val(result + ' : ' + pourcent + '%');
        } else {
            $('#result_ocr').val(symbols[class_max]+' : ' + pourcent + '%');
        }

    }
});
